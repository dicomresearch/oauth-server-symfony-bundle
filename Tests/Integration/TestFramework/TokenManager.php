<?php
/**
 * Created by PhpStorm.
 * User: alsu
 * Date: 16.08.15
 * Time: 13:28
 */

namespace OAuth2\ServerBundle\Tests\Integration\TestFramework;

use FileReader;
use Properties;
use Symfony\Component\Config\Definition\Exception\Exception;


class TokenManager {

    /**
     * @var TokenHelper
     */
    private $tokenHelper;

    /**
     * @var TokenManager
     */
    private $singleton;

    /**
     * @var Properties
     */
    private $props;

    public function getInstance($configFile=null)
    {
        if ($this->singleton == null)
            $this->singleton = new TokenManager();
        if ($configFile != null)
        {
            $this->singleton->setProperties($this->loadProperties($configFile));
        }
        return $this->singleton;
    }

    public function setProperties($props)
    {
        $props = new Properties();
        $this->props = $props;

    }

    public function getProperty($key, $defaultValue=null)
    {
        return $this->props->getProperty($key, $defaultValue);
    }

//    public function getIntProperty($key, $defaultValue=null)
//    {
//        return Integer.parseInt(getProperty(key, defaultValue));
//    }

    private static function loadProperties($configFile)
    {
        $props = new Properties();
        try
        {
            $props->load(new FileReader($configFile));
        }
        catch (Exception $e)
        {
            print($e);
        }
        return $props;
    }

    public function getTokenHelper()
    {
        if ($this->tokenHelper == null)
        {
            $this->tokenHelper = new TokenHelper($this);
        }
        return $this->tokenHelper;
    }

} 