<?php
/**
 * Created by PhpStorm.
 * User: alsu
 * Date: 16.08.15
 * Time: 13:40
 */

namespace OAuth2\ServerBundle\Tests\Integration\TestFramework;


use Symfony\Bundle\SecurityBundle\Tests\Functional\WebTestCase;
use OAuth2\ServerBundle\Tests\Integration\TestFramework\TokenManager;

class TokenHelper extends WebTestCase{

    /**
     * @var TokenManager
     */
    protected $app;

    protected $httpHost;
    protected $login;
    protected $password;
    protected $clientId;
    protected $scope;
    protected $state;
    protected $redirectUri;
    protected $clientSecret;

    public function __construct($app)
    {
        $this->app = $app;
        $this->httpHost = $this->app->getProperty("httpHost");
        $this->login = $this->app->getProperty("login");
        $this->password = $this->app->getProperty("password");
        $this->clientId = $this->app->getProperty("clientId");
        $this->scope = $this->app->getProperty("scope");
        $this->state = $this->app->getProperty("state");
        $this->redirectUri = $this->app->getProperty("redirectUri");
        $this->clientSecret = $this->app->getProperty("clientSecret");
    }

    public function  getAuthenticationCode()
    {
        $client = static::createClient();
        $client->followRedirects(false);
        $crawler = $client->request('GET', '/authorize?client_id='.$this->clientId.
            '&response_type=code&scope='.$this->scope.'&state='.$this->state.
            '&redirect_uri='.$this->redirectUri,
            array(),
            array(),
            array('HTTP_HOST' => $this->httpHost,
                'PHP_AUTH_USER' => $this->login,
                'PHP_AUTH_PW'   => $this->password,
            ));
        $uri = $client->getResponse()->headers->get('location');
        $code=null;
        $parseUrl = parse_url($uri);
        $query = parse_str($parseUrl['query'], $result);
        $code = $result['code'];
        return $code;
    }

    public function getAccessTokenByAuthenticationCode($code)
    {
        $code = $this->getAuthenticationCode();
        $client = static::createClient();
        $client->followRedirects(false);
        $client->request('POST', '/token',
            array('grant_type' => 'authorization_code',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'code' => $code,
                'redirect_uri' => $this->redirectUri),
            array(),
            array(
                'HTTP_HOST' => 'oauth.local'
            ));
        $crawler = $client->getResponse()->getContent();
        $crawler = json_decode($crawler);
        $access_token = $crawler->access_token;
        return $access_token;
    }

} 